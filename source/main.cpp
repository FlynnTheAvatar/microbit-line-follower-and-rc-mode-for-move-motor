#include "MicroBit.h"

MicroBit uBit;
MicroBitI2C i2c(I2C_SDA0, I2C_SCL0);

const uint8_t CHIP_ADDR = 0x62; // CHIP_ADDR is the standard chip address for the PCA9632, datasheet refers to LED control but chip is used for PWM to motor driver
const uint8_t MODE_1_REG_ADDR = 0x00; //mode 1 register address
const uint8_t MODE_2_REG_ADDR = 0x01;  //mode 2 register address
const uint8_t MOTOR_OUT_ADDR = 0x08;  //MOTOR output register address

const uint8_t MODE_1_REG_VALUE = 0x00; //setup to normal mode and not to respond to sub address
const uint8_t MODE_2_REG_VALUE = 0x04;  //Setup to make changes on ACK, outputs set to open-drain
const uint8_t MOTOR_OUT_VALUE = 0xAA;  //Outputs set to be controled PWM registers

const uint8_t MOTOR_LEFT = 0x00;
const uint8_t MOTOR_RIGHT = 0x01;

const uint8_t FORWARD = 0x00;
const uint8_t REVERSE = 0x01;

const uint8_t MOTOR_LEFT_OFFSET = 0x04; // Register offset for left motor
const uint8_t MOTOR_RIGHT_OFFSET = 0x02; // Register offest for right motor

const uint8_t leftImage[] = {
  1, 1, 0, 0, 0,
  1, 1, 0, 0, 0,
  1, 1, 0, 0, 0,
  1, 1, 0, 0, 0,
  1, 1, 0, 0, 0
};

const uint8_t rightImage[] = {
  0, 0, 0, 1, 1,
  0, 0, 0, 1, 1,
  0, 0, 0, 1, 1,
  0, 0, 0, 1, 1,
  0, 0, 0, 1, 1
};

const uint8_t middleImage[] = {
  0, 1, 1, 1, 0,
  0, 1, 1, 1, 0,
  0, 1, 1, 1, 0,
  0, 1, 1, 1, 0,
  0, 1, 1, 1, 0
};

const float q = 180.0 / 510.0;
const uint8_t fspeed = 64;
const uint8_t cspeed = 128;

volatile uint8_t speed = 64;
volatile bool isLineFollowerActive = true;

void writeI2C(uint8_t addr, uint8_t reg, uint8_t value)
{
#ifdef DEBUG2
  uBit.serial.printf("I2C Command: addr %d, reg %d, value %d, result: %d\r\n", addr, reg, value, i2c.writeRegister(addr << 1, reg, value));
#else
  i2c.writeRegister(addr << 1, reg, value);
#endif
}

/**
 * motor: MOTOR_LEFT (0), MOTOR_RIGHT (1)
 * direction: FORWARD (1), REVERSE (0)
 * speed: 0 - 255
 */
void motorOn(uint8_t motor, uint8_t direction, uint8_t speed)
{

  if (motor) {
    if (direction) {
      writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET, speed);
      writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET + 1, 0x00);
    } else {
      writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET, 0x00);
      writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET + 1, speed);
    }
  } else {
    if (direction) {
      writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET, 0x00);
      writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET + 1, speed);
    } else {
      writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET, speed);
      writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET + 1, 0x00);
    }
  }
}

/**
 * motor: MOTOR_LEFT (0), MOTOR_RIGHT (1)
 */
void motorOff(uint8_t motor)
{
  if (motor) {
    writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET, 0x00);
    writeI2C(CHIP_ADDR, MOTOR_RIGHT_OFFSET + 1, 0x00);
  } else {
    writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET, 0x00);
    writeI2C(CHIP_ADDR, MOTOR_LEFT_OFFSET + 1, 0x00);
  }
}

void onData(MicroBitEvent e)
{
  uint8_t data[3];
  uBit.radio.datagram.recv(data, 3);
  char s = data[0];
  uint8_t pitch = data[1];
  uint8_t roll = data[2];
#ifdef DEBUG2
  uBit.serial.printf("Data received: %c, %d, %d\r\n", s, pitch, roll);
#endif
  // enable line following
  if (s == 'a') {
    isLineFollowerActive = true;
    speed = fspeed;
  } else {
    // disable line following
    isLineFollowerActive = false;
    speed = cspeed;

    uBit.display.clear();

    if (s == 'x') {
      // so, we have a value from 0 to 255 for both roll and pitch
      motorOn(MOTOR_RIGHT, FORWARD, (255 - pitch + roll) * q);
      motorOn(MOTOR_LEFT, FORWARD, (pitch + roll) * q);
    }

    // forward
    if (s == 'f') {
      motorOn(MOTOR_RIGHT, FORWARD, speed);
      motorOn(MOTOR_LEFT, FORWARD, speed);
    }

    // back
    if (s == 'b') {
      motorOn(MOTOR_RIGHT, REVERSE, speed);
      motorOn(MOTOR_LEFT, REVERSE, speed);
    }

    // right
    if (s == 'r') {
      motorOff(MOTOR_RIGHT);
      motorOn(MOTOR_LEFT, FORWARD, speed);
    }

    // left
    if (s == 'l') {
      motorOn(MOTOR_RIGHT, FORWARD, speed);
      motorOff(MOTOR_LEFT);
    }

    if (s == 's') {
      motorOff(MOTOR_RIGHT);
      motorOff(MOTOR_LEFT);
    }
  }
}

int main()
{
  register uint8_t leftSensor;
  register uint8_t rightSensor;
  register int8_t proportional = 0, lastProportional = 0, derivative = 0;
  register int16_t integral = 0;
  register uint8_t kP = 3;
  float kI = 0.001;
  register uint8_t kD = 6;
  register int8_t adjustment;

  uBit.init();
#ifdef DEBUG
  uBit.serial.baud(57600);
#endif

  // init motors
  writeI2C(CHIP_ADDR, MODE_1_REG_ADDR, MODE_1_REG_VALUE);
  writeI2C(CHIP_ADDR, MODE_2_REG_ADDR, MODE_2_REG_VALUE);
  writeI2C(CHIP_ADDR, MOTOR_OUT_ADDR, MOTOR_OUT_VALUE);
  motorOff(MOTOR_RIGHT);
  motorOff(MOTOR_LEFT);

  // enable radio
  uBit.messageBus.listen(MICROBIT_ID_RADIO, MICROBIT_RADIO_EVT_DATAGRAM, onData);
  uBit.radio.setGroup(151);
  uBit.radio.enable();

  // main loop for handling sensor events
  while (true) {
    while (isLineFollowerActive) {
      leftSensor = uBit.io.P2.getAnalogValue();
      rightSensor = uBit.io.P1.getAnalogValue();
      proportional = leftSensor - rightSensor;
      derivative = proportional - lastProportional;
      integral = integral + proportional;
      lastProportional = proportional;

      adjustment = proportional * kP + integral * kI + derivative * kD;
      if (adjustment > speed) adjustment = speed;
      if (adjustment < -speed) adjustment = -speed;
#ifdef DEBUG
      uBit.serial.printf("prop: %d, derivative: %d, integral: %d, adjustment: %d\r\n", proportional, derivative, integral, adjustment);
#endif
      motorOn(MOTOR_LEFT, FORWARD, speed + adjustment);
      motorOn(MOTOR_RIGHT, FORWARD, speed - adjustment);

#ifdef DEBUG
      uBit.sleep(1000);
#else
      uBit.sleep(10);
#endif
    }
    uBit.sleep(1000);
  }

  release_fiber();
  return 0;
}
